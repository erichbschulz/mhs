
Potential sceope:

* APS database ("wardrownd app", "referral app")
* Pre-anaesthetic assessment
* Data collection for audit

Context
=======

MHS is a tertiary centre with a large suite of external and internally
developed clinical information systems.

The IT depart has a team of six developers and two staff supporting the HL7
messaging gateway and clinical data repository.

Architecture
============

Five layers:

1. Custom application (web-based, angular 1.4)
2. RESTful(ish) protocol
3. Server agent (C#)
4. Mixed legacy protocols (ODBC, HL7, other)
5. Legacy systems (Verdi, SQL Server, MCDR etc)

Initital scope - APS ward-round app
==============

Goals:

1. Produce a tablet friendly intuitive application allowing the APS team to
easily see APS referals and add structured notes on APS patients.

2. Gain experience in iterative agile collaborative (between clinicians and
    IT) clinical systems development.

3. Exlplore the HL7 Virtual Medical Record (VMR) format

Layer 2
=======

Services:

* authenticate
* list current APS patients
* detailed load on APS patient
* save structured note on patient
* log out
* look-up

Routes:
- log in
- list
- patient/:urn
