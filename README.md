This repo is for making notes about a potential application for MHS stuff.

The gulp configuration is based loosely on [this](http://stefanimhoff.de/2014/gulp-tutorial-2-development-server-browsersync-configuration/) tutorial.

Road map:
=========

1. operational APS ward-round tool
2. inclusion of pathology data
